<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h1>配列の削除・追加</h1>
  <?php
    $countries = array(10 => 'ブラジル', 'アルゼンチン', 'ペルー', 'ウルグアイ', 'ベネズエラ');

    echo "移動前<br>";
    print_r($countries);

    $a = array_slice($countries, 0, 1);
    // print_r($a);
    array_shift($countries);
    echo "<br>";
    print_r($countries);
    echo "<br>";
    array_push($countries, $a);
    print_r($countries);
  ?>
</body>
</html>
