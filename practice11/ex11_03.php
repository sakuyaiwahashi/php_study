<?php
/*---------------------------------
  演習11-3 Author:Iwahashi (copied from Okamoto's script)
  --------------------------------*/
// セッション開始
session_start();
// セッション開始ＩＤ新規変更：旧セッションデータ削除
session_regenerate_id(true);

$err_msg = []; //エラーメッセージ
$p_flg   = 0;  //POSTフラグ
$age     = ""; //年齢
$name    = ""; //名前

// セッション変数：名前
if (isset($_SESSION["name"])) {
    $name = $_SESSION["name"];
}

// セッション変数：年齢
if (isset($_SESSION["age"])) {
    $age = $_SESSION["age"];
}

//===== ポスト：リクエスト処理 =====
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  //--- ログイン処理 ---
  // POSTフラグ : ON
  $p_flg = 1;

  // 名前：HTML特殊文字変換
  $name = htmlspecialchars($_POST["name"], ENT_QUOTES);
  // 名前チェック
  if (!strlen($name)) {
    $err_msg[] = "名前が入力されていません";
  } else {
    // セッション変数：氏名
    $_SESSION["name"] = $name;
  }

  //年齢：HTML特殊文字変換
  $age = htmlspecialchars($_POST["age"], ENT_QUOTES);
  //名前チェック
  if (!strlen($age) || !is_numeric($age) || $age < 0) {
    $err_msg[] = "年齢が正しく入力されていません";
  } else {
      // セッション変数：年齢
      $_SESSION["age"] = $age;
  }

  //--- リダイレクト ---
  if (!count($err_msg)) {
    // リダイレクト：商品購入画面
    header("Location: ex11_04.php");
    // 終了
    exit;
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<h4>ログイン画面</h4>
<div id="err">
  <?php
    foreach ($err_msg as $val) {
      echo $val, "<br>";
    }
  ?>
</div>
<form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
  <div>
    お名前：
    <input type="text" name="name" value="<?= $name ?>" size="10">
    <br>
    年齢：
    <input type="text" name="age" value="<?= $age ?>" size="3">
    <br>
    <input type="submit" name="btn" value="ログイン">
  </div>
</form>
</body>
</html>
