<?php
    /*-------------------------------
        課題11-2 Author:Okamoto
      --------------------------------*/
    // セッション開始
    session_start();

    $err_msg = [];        // エラーメッセージ
    $p_flg   = 0;            // POSTフラグ

    //===== ポスト：リクエスト処理  =====
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        //--- 切断処理 ---
        // POSTフラグ：ＯＮ
        $p_flg = 1;
        // 全セッション変数削除
        $_SESSION = [];
        // クッキーのセッションキー削除（無効）
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time() - 42000, '/');
        }
        // セッション破棄
        session_destroy();
        $msg = "切断しました";

        //===== 初期（GET）：リクエスト処理  =====
    } else {
        //--- セッション情報の編集 ---
        $msg = "";
        // セッション情報取得ループ
        foreach ($_SESSION as $key => $val) {
            // セッション「変数」 値
            $msg .= "[$key] $val<br />";
        }
        // セッションＩＤの編集
        if (isset($_COOKIE[session_name()])) {
            $ses_name = $_COOKIE[session_name()];
        } else {
            $ses_name = "";
        }
        $msg .= "クッキーのセッションID：" . $ses_name;
    }
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ex11_02.php</title>
    <style>
        <!--
        #err {
            color : red;
        }

        -->
    </style>
</head>
<body>
<h4>セッション変数表示</h4>
<div id="err">
    <?php
        // エラーメッセージ表示
        foreach ($err_msg as $val) {
            echo $val, "<br />";
        }
    ?>
</div>
<?php
    // セッション情報表示
    echo $msg;
    // 初期（GET）リクエスト画面作成
    if (!$p_flg) {
        ?>
        <form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
            <div>
                <br/>
                <input type="submit" name="btn" value="切断"/>　
                <input type="hidden" name="zzz" value="<?php time(); ?>"/>　
            </div>
        </form>
        <?php
    }
?>
</body>
</html>
