<?php
    /*-------------------------
        演習11-1 Author:Okamoto
      --------------------------*/
    // セッション開始
    session_start();

    // セッション：現在時刻
    $_SESSION["time"] = time();
    // セッション：セッションＩＤ
    $_SESSION["session_id"] = session_id();

    // リダイレクト：セッション情報表示画面
    header("Location: ex11_02.php");
    // 終了
    exit;