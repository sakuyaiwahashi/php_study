<?php
    /*----------------------------
        演習11-3 Author:Okamoto
      ----------------------------*/
    // セッション開始
    session_start();
    // セッション開始ＩＤ新規変更：旧セッションデータ削除
    session_regenerate_id(true);

    $err_msg = [];        // エラーメッセージ
    $p_flg   = 0;            // POSTフラグ
    $age     = "";            // 名前
    $name    = "";            // 年齢

    // セッション変数：名前
    if (isset($_SESSION["name"])) {
        $name = $_SESSION["name"];
    }
    // セッション変数：年齢
    if (isset($_SESSION["age"])) {
        $age = $_SESSION["age"];
    }

    //===== ポスト：リクエスト処理  =====
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //--- ログイン処理 ---
        // POSTフラグ：ＯＮ
        $p_flg = 1;

        // 名前：HTML特殊文字変換
        $name = htmlspecialchars($_POST["name"], ENT_QUOTES);
        // 名前チェック
        if (!strlen($name)) {
            $err_msg[] = "名前が入力されていません";
        } else {
            // セッション変数：氏名
            $_SESSION["name"] = $name;
        }

        // 年齢：HTML特殊文字変換
        $age = htmlspecialchars($_POST["age"], ENT_QUOTES);
        // 名前チェック
        if (!strlen($age) || !is_numeric($age) || $age < 0) {
            $err_msg[] = "年齢が正しく入力されていません";
        } else {
            // セッション変数：年齢
            $_SESSION["age"] = $age;
        }

        //--- リダイレクト ---
        if (!count($err_msg)) {
            // リダイレクト：商品購入画面
            header("Location: ex11_04.php");
            // 終了
            exit;
        }
    }
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ex11_03.php</title>
    <style>
        <!--
        #err {
            color : red;
        }

        -->
    </style>
</head>
<body>
<h4>ログイン画面</h4>
<div id="err">
    <?php
        // エラーメッセージ表示
        foreach ($err_msg as $val) {
            echo $val, "<br />";
        }
    ?>
</div>
<form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
    <div>
        お名前：<input type="text" name="name" value="<?= $name ?>" size="10"/><br/>
        年　齢：<input type="text" name="age" value="<?= $age ?>" size="3"/><br/>
        <br/>
        <input type="submit" name="btn" value="ログイン"/>
    </div>
</form>
</body>
</html>
