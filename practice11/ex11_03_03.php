<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h2>ログイン画面</h2>
  <?php if($_SERVER["REQUEST_METHOD"] !== "POST"):?>
  <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
  お名前：
  <input type="text" name="name" value="">
  <br>
  年齢：
  <input type="text" name="age" value="">
  <input type="submit" name="" value="ログイン">
  <?php else:?>
  <?php
    $_SESSION["name"] = $_POST["name"];
    $_SESSION["age"] = $_POST["age"];
    header("Location: ex11_04.php");
    exit;
  ?>
  <?php endif;?>
</body>
</html>
