<?php
  /*--------------------------------------------------------
    演習11-6 Author:Iwahahsi (copied from okamot's script)
  --------------------------------------------------------*/
  $err_msg = [];        // エラーメッセージ
  $referer = '';        // リンク元・POST送信元URL（画面）
  $name = '';

  // セッション開始
  session_start();

  // ポスト送信元のＵＲＬ取得
  if (isset($_SERVER["HTTP_REFERER"])) {
    // ＵＲＬの引数部（？）を分離
    // （引数例 :localhost/...）
    list($referer) = explode("?", $_SERVER["HTTP_REFERER"]);
  }

  // 送信元ＵＲＬの/分割
  $referer = explode("/", $referer);

  if (array_pop($referer) === "ex11_05.php"
      // 送信元ＵＲＬ：カート表示画面
     &&
     ($_SERVER["REQUEST_METHOD"] == "POST")
     // リクエスト：ＰＯＳＴ
     &&
     ($_POST["btn"] == "精算する")
     // 精算ボタン
     &&
     isset($_SESSION["name"])
     // セッション変数：名前
     &&
     count($_SESSION["cart"])
     // カート商品数
     ) {
     // セッション変数名前取得
     $name = $_SESSION["name"];
   } else {
     $err_msg[] = "精算できません<br>ログインしてください";
   }
   //	echo $_SERVER["REQUEST_METHOD"]," ",$_POST["btn"]," ",isset($_SESSION["name"])," ",count($_SESSION["cart"]);

  if (count($err_msg) == 0) {
    //--- カート商品表示（編集）---
    $tbl = "<tr><th>商品名</th><th>数量</th></tr>";

    // セッション変数：カート商品配列キーループ
    foreach ($_SESSION["cart"] as $key => $val) {
      $tbl .= "<tr>";
      foreach ($val as $val2) {
        // 商品名、数量
        $tbl .= "<td>$val2</td>";
      }
      $tbl .= "</tr>";
    }
    // セッション変数削除
    $_SESSION = [];
    if (isset($_COOKIE[session_name()])) {
      // クッキー削除：セッション名（ＩＤ）、パス（/: 前フォルダ）指定
      setcookie(session_name(), "", time() - 42000, '/');
    }
    // セッション破棄
    session_destroy();
  }
?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h4>精算完了</h4>
  <div id="err">
    <?php
      // エラーメッセージ表示
      foreach ($err_msg as $val) {
        echo $val, "<br>";
      }

      // セッション有効判断
      if (count($err_msg)):
        // セッション無効：ログイン画面リンク設定
        echo '<br>[<a href="ex11_03.php">ログイン</a>]';
      else:
        // セッション有効：精算完了表示
    ?>
  </div>

  <?= $name ?>様<br><br>

  <div>
    <table border="1">
      <?= $tbl ?>
    </table>
    <br>お買い上げありがとうございました。
  </div>
<?php endif;?>
</body>
</html>
