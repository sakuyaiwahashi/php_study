<?php
    /*---------------------------
        課題11-4 Author:Okamoto
     ----------------------------*/
    // セッション開始
    session_start();

    $err_msg      = [];        // エラーメッセージ
    $p_flg        = 0;            // POSTフラグ
    $s_flg        = 0;            // セッションフラグ
    $name         = "";            // 名前
    $product_name = "";            // 商品名
    $num          = "";            // 数量

    // セッション変数：名前
    if (isset($_SESSION["name"])) {
        // 氏名
        $name = $_SESSION["name"];
        // セッションフラグ：ＯＮ
        $s_flg = 1;
    } else {
        $err_msg[] = "ログインしてください";
    }
    // セッション変数：年齢
    if (isset($_SESSION["age"])) {
        // 年齢
        $age = $_SESSION["age"];
    }

    //===== ポスト：リクエスト処理  =====
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //--- 「カートに入れる」ボタン処理  ---
        if ($_POST["btn"] == "カートに入れる") {
            // POSTフラグ：ＯＮ
            $p_flg = 1;
            // 入力チェック
            // 商品名：HTML特殊文字変換
            $product_name = htmlspecialchars($_POST["product_name"], ENT_QUOTES);
            // 商品チェック
            if (!strlen($product_name)) {
                $err_msg[] = "商品名が入力されていません";
            }
            // 数量：HTML特殊文字変換
            $num = htmlspecialchars($_POST["num"], ENT_QUOTES);
            // 数量チェック
            if (!strlen($num) || !is_numeric($num) || $num == 0) {
                $err_msg[] = "数量が正しく入力されていません";
            }
            //--- セッション「カート配列ト」：商品追加処理  ---
            if (!count($err_msg)) {
                $up_flg = false;        // 数量更新フラグ：OFF
                // 「カート配列」：同一商品名検索
                if (isset($_SESSION["cart"])) {
                    foreach ($_SESSION["cart"] as $key => $val) {
                        // 商品名チェック
                        iF ($product_name === $val[0]) {
                            // 商品数量カウントアップ
                            $_SESSION["cart"][$key][1] += $num;
                            $up_flg                    = true;        // 数量更新フラグ：ON
                            break;
                        }
                    }
                }
                // 「カート配列」 商品追加 <-- 商品配列 [0]：商品名、[1]：数量
                if ($up_flg === false) {
                    $_SESSION["cart"][] = [
                        $product_name,
                        $num,
                    ];
                }
                // 商品名、数量：クリア
                $product_name = "";
                $num          = "";
            }
        } //--- 「カートを見る」ボタン処理  ---
        else {
            // セッション変数：カートの製品数判断
            if (isset($_SESSION["cart"]) && count($_SESSION["cart"])) {
                // リダイレクト：カート内容表示画面
                header("Location: ex11_05.php");
                exit;
            } else {
                $err_msg[] = "製品が１つも入力されていません";
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ex11_04.php</title>
    <style>
        <!--
        #err {
            color : red;
        }

        -->
    </style>
</head>
<body>
<h4>商品購入</h4>
<div id="err">
    <?php
        // エラーメッセージ表示
        foreach ($err_msg as $val) {
            echo $val, "<br />";
        }
        // セッション有効判断
        if (!$s_flg)
        {
            // セッション無効：ログイン画面リンク設定
            echo '<br/>[<a href="ex11_03.php">ログイン</a>]';
        }
        else
        {
    ?>
</div>
<?= $name ?>様<br/><br/>
<form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
    <div>
        商品名<br/>
        <input type="text" name="product_name" value="<?= $product_name ?>" size="15"/><br/>
        数量<br/>
        <input type="text" name="num" value="<?= $num ?>" size="5"/><br/>
        <br/>
        <input type="submit" name="btn" value="カートに入れる"/><br/>
        <br/>
        <input type="submit" name="btn" value="カートを見る"/>
    </div>
</form>
<?php } ?>
</body>
</html>
