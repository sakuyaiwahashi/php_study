<?php
  /*-----------------------------
    演習11-5 Author:Iwahashi (copied by Okamoto's script)
  ------------------------------*/
  /*-----------------------------
    セッションのカート商品配列と削除ボタンの関連イメージ
      カート商品配列：$_SESSION["cart"]のデータ構成
      例）
      「配列キー」
      0 => array(0 => string 'テレビ', 1 => string '1' (数量))
      1 => array(0 => string)

    ------------------------------*/
  // セッション開始
  session_start();

  $err_msg = [];      // エラーメッセージ
  $p_flg   = 0;       // POSTフラグ
  $s_flg   = 0;       // セッションフラグ
  $name    = '';
  $tbl     = '';

  // セッション変数チェック：名前、カート
  if (isset($_SESSION["name"])) {
    if(isset($_SESSION["cart"]) && count($_SESSION["cart"])) {
      // セッション変数：名前
      $name = $_SESSION["name"];

      // セッションフラグ：ＯＮ
      $s_flg = 1;
    } else {
      $err_msg[] = "製品が１つも入力されてません";
    }
  } else {
    $err_msg[] = "ログインしてください";
  }

  //===== ポスト：リクエスト処理 =====
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // POSTフラグ：ＯＮ
    $p_flg = 1;

    //--- セッション・カート「商品」配列：商品削除 ---
    if (in_array("削除", $_POST, true)) {
      // 削除ボタンnameの取り出し 【３つめのボタンの場合：$_POST['del_btn'] => '削除'】
      $del_btn = array_search("削除", $_POST, true);

      // 「商品」配列：削除配列キー（添え字）
      list(, $del_no) = explode("del_btn", $del_btn);

      // 「商品」配列：配列キー（添え字）振り直し
      $_SESSION["cart"] = array_merge($_SESSION["cart"]);

      // セッション変数：カート商品数判断
      if (count($_SESSION["cart"]) == 0) {
        // リダイレクト：商品購入画面
        header("Locatio: ex11_04.php");
        exit;
      }
    }
  }

  if (count($err_msg) == 0) {
    //--- カート商品表示（編集） ---
    $tbl = "<tr><th>商品名</th><th>数量</th><th> * </th></tr>";

    // セッション変数：カート商品配列キーループ
    foreach ($_SESSION["cart"] as $key => $val) {
      $tbl .= "<tr>";
      // 数量取得
      foreach ($val as $val2) {
        // 商品名、数量
        $tbl .= "<td>$val2</td>";
      }
      // 商品名別削除ボタン：「del_btnカート商品配列キー」
      $tbl .= '<td><input type="submit" name="del_btn' . $key . '" value="削除"></td>';

      $tbl .= "</tr>";
    }
  }
  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h4>カート表示</h4>
  <div id="err">
    <?php
      //エラーメッセージ表示
      foreach ($err_msg as $val) {
        echo $val, "<br>";
      }
      // echo $_SERVER["HTTO_REFERER"];
      // セッション有効判断
      if (!$s_flg):
        echo '<a href="ex11_03.php">ログイン</a>';
      else:
     ?>

     <?= $name ?>様

     <form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
       <div>
         <table border="1">
           <?= $tbl ?>
         </table>
         <br>
       </div>
     </form>

     <form action="ex11_04.php" method="get">
       <input type="submit" name="btn" value="まだまだ購入">
     </form>
     <br>

     <form action="ex11_06.php" method="post">
       <input type="submit" name="btn" value="精算する">
     </form>
   <?php endif; ?>

  </div>
</body>
</html>
