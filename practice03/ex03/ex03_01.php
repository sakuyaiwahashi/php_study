<?php
/*
 演習3-1
 Author:Okamoto
 
 必要なとき以外は<head>は省略しています。。。

下記の表の国を 和製国名 : 国名 の型で表示しなさい
 
*/
?>
<html>
<head>
    <meta http-equiv="Content-Style-Type" content="text/css">
    <style type="text/css">
        <!--
        -->
    </style>

    <title>ex03_01.php</title>
</head>
<body>
<h4>国名表</h4>
<?php
    $country = array(
                    "越" => "ベトナム",
                    "正" => "スリランカ",
                    "緬" => "ミャンマー",
                    "比" => "フィリピン",
                    "尼" => "インドネシア",
                    "蒙" => "モンゴル",
                    "老" => "ラオス",
                    "泰" => "タイ",
                    "馬" => "マレーシア",
                );

    foreach ($country as $key => $val) {
        echo "$key : $val<br />";
    }

?>
</body>
</html>
