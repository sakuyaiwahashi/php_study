<?php
/*
 演習3-3
   Author:Okamoto
 必要なとき以外は<head>は省略しています。。。
*/
?>
<html>
<head>
    <meta http-equiv="Content-Style-Type" content="text/css">
    <style type="text/css">
        <!--
        -->
    </style>

    <title>ex03_03.php</title>
</head>
<body>
<h4>演習3-3：回数カウント</h4>
<?php
    $arr = array();

    for ($i = 0; $i < 100; $i++) {
        $arr[] = rand(1, 50);
        echo "$arr[$i] ";
        if ($i % 10 === 9) {
            echo "<br />";
        }
    }

    echo "-----------------------------------------<br />";

    for ($i = 1; $i <= 50; $i++) {
        $cnt = 0;
        foreach ($arr as $val) {
            if ($val === $i)
                $cnt++;
        }
        echo "$i:$cnt ";
        if ($i % 10 === 0) {
            echo "<br />";
        }
    }
?>
</body>
</html>
