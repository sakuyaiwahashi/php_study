<?php
/*
 演習3-7
   Author:Okamoto
 必要なとき以外は<head>は省略しています。。。
*/
?>
<html>
<head>
    <meta http-equiv="Content-Style-Type" content="text/css">
    <style type="text/css">
        <!--
        td {
            width: 60px;
            text-align: center;
        }

        th {
            width: 60px;
            text-align: center;
        }

        -->
    </style>

    <title>ex03_04.php</title>
</head>
<body>
<h4>２次元配列の表示</h4>
<?php
    $schedule = array(
                    4 => array(
                            10 => "入校式",
                            11 => "オリエンテーション",
                            20 => "防火避難訓練",
                            22 => "人権教育"
                        ),
                    5 => array(
                            13 => "人権教育"
                        ),
                    6 => NULL,
                    7 => array(
                            1 => "安全講話",
                            17 => "人権教育"
                        ),
                    8 => NULL,
                    9 => array(
                            23 => "職業講話"
                        ),
                );

    foreach ($schedule as $key => $val) {
        if (is_array($val)) {
            echo "[$key] 　";
            foreach ($val as $key2 => $val2) {
                echo "[$key2] $val2 　";
            }
        } else {
            echo "[$key] $val";
        }
        echo "<br />";
    }
?>
</body>
</html>
