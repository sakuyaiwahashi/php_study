<?php
/*
 演習3-8
   Author:Okamoto
 必要なとき以外は<head>は省略しています。。。
*/
?>
<html>
<head>
    <meta http-equiv="Content-Style-Type" content="text/css">
    <style type="text/css">
        <!--
        td {
            width: 60px;
            text-align: center;
        }

        th {
            width: 60px;
            text-align: center;
        }

        -->
    </style>
    <title>ex03_05.php</title>
</head>
<body>
<h4>２次元配列の表示</h4>
<?php
    $arr1 = array();
    $arr2 = array();
    $col1 = 0;
    $col2 = 0;
    $val = 0;

    for ($i = 0; $i < 20; $i++) {
        $col1 = rand(0, 9);
        $col2 = rand(0, 50);
        $val = rand(1, 100);
        echo "$col1 $col2 $val <br />";
        $arr1[$col1][$col2] = $val;
    }
    ksort($arr1, SORT_REGULAR);

    echo "<pre>";

    foreach ($arr1 as $col1 => $arr2) {
        ksort($arr2, SORT_REGULAR);
        foreach ($arr2 as $col2 => $val) {
            echo "[$col1][$col2]：$val  ";
        }
        echo "<br />";
    }

    echo "</pre>";
?>
</body>
</html>
