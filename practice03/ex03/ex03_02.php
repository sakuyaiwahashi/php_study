<?php
/*
 演習3-2
   Author:Okamoto
 必要なとき以外は<head>は省略しています。。。

10個の乱数を表示し、基準値（乱数で取得）と比較して、基準値より大きい数字のみ表示する
 
*/
?>
<html>
<head>
    <meta http-equiv="Content-Style-Type" content="text/css">
    <style type="text/css">
        <!--
        -->
    </style>

    <title>ex03_02.php</title>
</head>
<body>
<h4>演習3-2：基準との比較</h4>
<?php
    $num = array();
    for ($i = 0; $i < 10; $i++) {
        echo ($num[] = rand(0, 100)) . " ";
    }

    do {
        $base = rand(0, 100);
        echo "<br />基準{$base}より大きい値は、";

        foreach ($num as $val) {
            if ($val > $base)
                echo "$val ";
        }
    } while ($base > 10);
?>
</body>
</html>
