<?php

  $db = new PDO("mysql:host=localhost;dbname=ec_kaede", "root", "root");

  //お勧め商品のデータベース取得
  $sql = 'SELECT * FROM items WHERE rec_flg=1';
  $result = $db -> query($sql);
  $rows = $result -> fetchall(PDO::FETCH_ASSOC);

  //お勧めじゃない商品のデータベース取得
  $sql2 = 'SELECT * FROM items WHERE rec_flg=0';
  $result2 = $db -> query($sql2);
  $rows2 = $result2 -> fetchall(PDO::FETCH_ASSOC);

?>
<?php
    // ヘッダーメタをインクルード
    include('./header_meta.php');
?>
<body>

  <?php
  // ヘッダーをインクルード
  include('./header.php');
  ?>

  <div id="wrapper">


    <?php
      // キービジュアルをインクルード
      include('top_keyvisual.php');
    ?>

    <div class="main-width">

    <?php
    // おすすめ商品をインクルード
    include('top_recommend.php');
    ?>

<!-- ここの高さ揃えるの直してください -->
      <div id="contents" class="clearfix">
        <div id="items" class="clearfix">
          <ul class="clearfix">
            <?php foreach ($rows2 as $row):?>
            <li>
              <a href="detail.php?id=<?= $row['id']?>"><img src="<?= $row['image_path']?>" alt="<?= $row['name']?>"></a>
              <a href="detail.php?id=<?= $row['id']?>"><p><?= $row['name']?></p></a>
              <p>¥<?= $row['price']?></p>
            </li>
            <?php endforeach;?>
          </ul>
            <a href="#">もっと見る</a>
        </div>
        </form>
        <?php
          // サイドバーをインクルード
          include('sidebar.php');
        ?>
        </div>

        <?php
        // フッターをインクルード
        include('footer.php');
        ?>
      </div>
    </div>


</body>
</html>
