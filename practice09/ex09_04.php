<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h2>文字列検索置換</h2>
  <?php if($_SERVER["REQUEST_METHOD"] !== "POST"):?>
  <!-- GETで要求された場合 -->
    <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
      検索対象文字列
      <br>
      <input type="text" name="search1" value="">
      <br>
      検索文字列
      <input type="text" name="search2" value="">
      置換文字列
      <input type="text" name="search3" value="">
      <input type="submit" name="" value="送信">
    </form>
  <?php else:?>

  <?php
    $search1 = $_POST['search1'];
    $search2 = $_POST['search2'];
    $search3 = $_POST['search3'];

    if ($search1 === '' || $search2 === '' || $search3 === '') {
      echo "入力値が正しくありません";
    }else{
    $result =str_replace($search2, $search3, $search1);

    echo $result;
    }
  ?>
  <?php endif; ?>
</body>
</html>
