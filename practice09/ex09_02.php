<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>

  <?php if($_SERVER["REQUEST_METHOD"] !== "POST"):?>
  <!-- GETで要求された場合 -->
    <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
      実数：<input type="text" name="data" value="">
      <input type="submit" name="" value="送信">
    </form>
  <?php else:?>
  <?php
    $data = $_POST['data'];
    printf("整数：%'*8d", $data);
    echo "<br>";
    printf("実数：%'*8.2f", $data);
    echo "<br>";
    printf("文字:%'*-8s", $data);
  ?>
  <?php endif; ?>

</body>
</html>
