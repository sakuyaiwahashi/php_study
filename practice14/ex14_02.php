<?php
  $p_flg = 0;
  $path = getcwd();
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $p_flg = 1;
    $newFolder = $_POST['newFolder'];
    $ok = mkdir($newFolder);

  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h2>フォルダ作成</h2>
  <form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
    新規フォルダ名
    <input type="text" name="newFolder">
    <br>
    <input type="submit" value="作成">
  </form>
  <?php if($p_flg == 1 || $ok == TRUE):?>
  <?php echo $path . $newFolder; ?>
<?php elseif ($p_flg == 1 || $ok == FALSE):?>
  <?php echo $newFolder . "フォルダを作成できませんでした。"?>
  <?php endif;?>
</body>
</html>
