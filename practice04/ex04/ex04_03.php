<?php
/*
 演習4-3
   Author:Okamoto*/
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ex04_03.php</title>
</head>
<body>
<h4>計算結果</h4>
<?php
    $num1 = $_POST["num1"];
    $num2 = $_POST["num2"];
    $operator = $_POST["operator"];
    if (is_numeric($num1) && is_numeric($num2)) {
        switch ($operator) {
            case "+":
                $ans = $num1 + $num2;
                break;
            case "-":
                $ans = $num1 - $num2;
                break;
            case "*":
                $ans = $num1 * $num2;
                break;
            case "/":
                if ($num2 <> 0) {
                    $ans = $num1 / $num2;
                }
                break;
            case "%":
                if ($num2 <> 0) {
                    $ans = $num1 % $num2;
                }
                break;
        }
    }
    if (isset($ans)) {
        echo $num1, " $operator ", $num2, " = ", $ans;
    } else {
        echo "正しく入力してください";
    }
?>
</body>
</html>
