<?php
/*
 演習4-4
   Author:Okamoto
*/
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ex04_04.php</title>
</head>
<body>
<h4>計算＆計算結果</h4>

<?php
    $num1 = $_POST["num1"];
    $num2 = $_POST["num2"];
    $operator = $_POST["operator"];

    if (is_numeric($num1) && is_numeric($num2)) {
        switch ($operator) {
            case "+":
                $ans = $num1 + $num2;
                break;
            case "-":
                $ans = $num1 - $num2;
                break;
            case "*":
                $ans = $num1 * $num2;
                break;
            case "/":
                if ($num2 <> 0)
                    $ans = $num1 / $num2;
                break;
            case "%":
                if ($num2 <> 0)
                    $ans = $num1 % $num2;
                break;
        }
    }

    if (!isset($ans)) {
        $ans = "";
        $err = "正しく入力してください";
    } else {
        $ans = "= " . $ans;
        $err = "";
    }

?>

<form action="ex04_04.php" method="post">
    <div>
        <input type="text" name="num1" value="<?php echo $num1; ?>" size="5"/>
        <input type="text" name="operator" value="<?php echo $operator; ?>" size="2"/>
        <input type="text" name="num2" value="<?php echo $num2; ?>" size="5"/>
        <?php echo $ans; ?>
    </div>
    <div><input type="submit" value="送信"/></div>
    <div><?php echo $err; ?></div>
</form>
</body>
</html>
