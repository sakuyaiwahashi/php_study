<?php
/*
 演習4-2
   Author:Okamoto
*/
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>ex04_02.php</title>
</head>
<body>
<h4>入力結果表示</h4>
<?php

    $err_flg = 0;
    $name = $_POST["name"];
    $age = $_POST["age"];

    if ($name == "") {
        echo "名前が入力されていません<br />";
        $err_flg += 1;
    }

    if ($age == "") {
        echo "年齢が入力されていません<br />";
        $err_flg += 1;
    } else {
        if (!is_numeric($age)) {
            echo "年齢が数値ではありません<br />";
            $err_flg += 1;
        }
    }

    if (!$err_flg) {
        echo $_POST["age"], "歳の", $_POST["name"], "さん、いらっしゃい";
    }

?>
</body>
</html>
