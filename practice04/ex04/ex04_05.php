<?php
/*
 演習4-5
   Author:Okamoto
*/
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <style type="text/css">
        <!--
        th {
            vertical-align: top;
        }

        -->
    </style>
    <title>ex04_05.php</title>
</head>
<body>

<?php
    $name = $_POST["name"];
    if ($name == "") {
        $name = '入力なし';
    }

    $address = $_POST["address"];
    if ($address == "") {
        $address = '入力なし';
    }

    if (isset($_POST["age"])) {
        $age = $_POST["age"];
    } else {
        $age = '入力なし';
    }

    $same_town = "";
    if (isset($_POST["same_town"])) {
        foreach ($_POST["same_town"] as $key => $val) {
            $same_town .= $val . " ";
        }
    } else {
        $same_town = '入力なし';
    }
?>

<table>
    <tr>
        <th>氏名：</th>
        <td><?php echo $name; ?></td>
    </tr>
    <tr>
        <th>住所：</th>
        <td><?php echo "<pre>$address<pre>"; ?></td>
    </tr>
    <tr>
        <th>年齢：</th>
        <td><?php echo $age; ?></td>
    </tr>
    <tr>
        <th>同居：</th>
        <td><?php echo $same_town; ?></td>
    </tr>
</table>
</body>
</html>
