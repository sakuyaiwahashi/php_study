<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
    date_default_timezone_set('Asia/Manila');
    echo date("y年m月d日 H時i分s秒 (l)");
  ?>
</body>
</html>
