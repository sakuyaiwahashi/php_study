<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php if($_SERVER["REQUEST_METHOD"] !== "POST"):?>
  // GETで要求された場合
    <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
      誕生日：<input type="text" name="month">月
      <input type="text" name="day">日
      <input type="submit" value="送信">
    </form>
  <?php else:?>
  // フォームからPOSTによって要求された場合

  <?php
    $month = $_POST['month'];
    $day = $_POST['day'];

    // var_dump( checkdate( 13,24,2005 ) );
    // echo $month;
    // echo $day;
  ?>

  <?php if($month === "" || $day ===""):?>
    <br>
    月、日どちらも入力してください。
    <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
      誕生日：<input type="text" name="month">月
      <input type="text" name="day">日
      <input type="submit" value="送信">
    </form>
  <?php elseif(is_numeric($month) === FALSE || is_numeric($day) === FALSE):?>
    <br>
    入力値が数値ではありません。
    <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
      誕生日：<input type="text" name="month">月
      <input type="text" name="day">日
      <input type="submit" value="送信">
    </form>
  <?php elseif(checkdate ( $month, $day, 2000) === FALSE):?>
    <br>
    ありえない日付です。
    <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
      誕生日：<input type="text" name="month">月
      <input type="text" name="day">日
      <input type="submit" value="送信">
    </form>
  <?php else:?>
  <?php
  $year = 2017;
  var_dump($year);

    if($month <= 9){
      $month = "0".$month;
    }
    if($day <=9){
      $day = "0".$day;
    }
    for($i=1; $i<=5; $i++ ){
      echo "<br>";
      echo "${i}回目：";
      $year += 1;
      echo date("Y/m/d(l)", strtotime("${year}${month}${day}"));
    }
    // echo date("Y/m月d日は、", strtotime("${year}${month}${day}"));
  ?>
  <?php endif;?>
  <?php endif; ?>
</body>
</html>
