<?php
  class TimeStampDate {
    //----------------
    // メンバー変数
    //----------------
    private $stamp;  //タイムスタンプ

    //======メソッド=====
    //---------------------
    //    コンストラクタ
    //---------------------
    public function __construct()
    {
      $this->stamp = time();
    }

    //---------------------
    //  タイムスタンプ取得
    //---------------------

    public function getStamp()
    {
      return $this->stamp;
    }

    //---------------------
    //  タイムスタンプ編集
    //---------------------

    public function convertDate()
    {
      // タイムゾーン：日本
      date_default_timezone_set('Asia/Manila');

      // タイムスタンプ編集
      return date("Y-m-d H:i:s", $this->stamp);
    }


  }
