<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php if($_SERVER["REQUEST_METHOD"] !== "POST"): //GETで要求された場合?>
    <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
      <input type="text" name="calorie" value="">一日に必要なカロリー
      <br>
      <input type="text" name="carbs" value="">一日に必要な炭水化物
      <br>
      <input type="text" name="protein" value="">一日に必要なタンパク質
      <br>
      <input type="text" name="fats" value="">一日に必要な脂質
      <br>
      <input type="text" name="times" value="">食事回数
      <br>
      <input type="submit" name="" value="送信">
      <br>
      <br>
      <br>

      <input type="text" name="oatmeal" value="">オートミール摂取量
      <br>
      <input type="text" name="carbs" value="">プロテインパウダー摂取量
      <br>
      <input type="text" name="protein" value="">卵の摂取個数
      <br>
      <input type="text" name="fats" value="">マヨネーズ摂取量
      <br>
      <input type="text" name="times" value="">食事回数
      <br>
      <input type="submit" name="" value="送信">
    </form>
  <?php else: // フォームからPOSTによって要求された場合?>
    <?php
      //入力された値を取得
      $calorie = $_POST['calorie'];
      $carbs = $_POST['carbs'];
      $protein = $_POST['protein'];
      $fats = $_POST['fats'];

      //オートミール・プロテインパウダー・卵・マヨネーズの1gあたりの栄養素を代入

      //オートミール（$oat_）
      $oat_calorie = 4.08; //kcal
      $oat_carbs = 0.736; //g
      $oat_protein = 0.117; //g
      $oat_fats = 0.074; //g

      //プロテインパウダー($pro_)
      $pro_calorie = 3.71; //kcal
      $pro_carbs = 0.000001; //g
      $pro_protein = 0.657; //g
      $pro_fats = 0.057; //g

      //卵 1個($egg_)
      $egg_calorie = 91; //kcal
      $egg_carbs = 0.18; //g
      $egg_protein = 7.74; //g
      $egg_fats = 6; //g

      //マヨネーズ ($mayo_)
      $mayo_calorie = 7; //kcal
      $mayo_carbs = 0.000001; //g
      $mayo_protein = 0.000001; //g
      $mayo_fats = 0.75; //g

      ?>
  <?php endif;?>
</body>
</html>
