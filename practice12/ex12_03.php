<?php
  $p_flg = 0;
  $radius = 0;
  $diameter = 0;
  $circumference = 0;
  $area = 0;
  $pi = 3.14;

  function cal($radius, $pi){
    $diameter = 2 * $radius;
    $circumference = 2 * $radius * $pi;
    $area = $pi * $radius * $radius;

    $result = array(
      "半径" => $radius,
      "直径" => $diameter,
      "円周" => $circumference,
      "面積" => $area);

    return $result;
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $p_flg = 1;
    $radius = $_POST['radius'];
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h2>円のいろいろ(配列リターン値)</h2>
  <form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
    <input type="text" name="radius" value="<?= $radius ?>">
    <input type="submit" value="計算">
  </form>
  <?php if($p_flg == 1):?>
  <?php print_r(cal($radius, $pi)) ?>
  <?php endif;?>
</body>
</html>
