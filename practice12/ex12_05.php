<?php

  $p_flg = 0;
  $year = 0;
  $month = 0;
  $day = 0;

  function calDay($year, $month, $day) {
    $weekday = array("日", "月", "火", "水", "木", "金", "土");
    $w = date("w", strtotime("${year}-${month}-${day}"));
    echo date("Y年m月d日は、", strtotime("${year}-${month}-${day}"));
    echo $weekday[$w]. "曜日です。";
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $p_flg = 1;
    $year = $_POST['year'];
    $month = $_POST['month'];
    $day = $_POST['day'];

    if ($month <= 9) {
      $month = "0" . $month;
    }

    if ($day <= 9) {
      $day = "0" . $day;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h2>指定年月日の曜日表示</h2>
  <form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
    年月日：
    <input type="text" name="year" value="<?= $year ?>">年
    <input type="text" name="month" value="<?= $month ?>">月
    <input type="text" name="day" value="<?= $day ?>">日
    <input type="submit" value="送信">
  </form>
  <?php if($p_flg == 1):?>
  <?php calDay($year, $month, $day) ?>
  <?php endif;?>
</body>
</html>
