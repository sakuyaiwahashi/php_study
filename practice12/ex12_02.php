<?php
$p_flg = 0;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $p_flg = 1;
  $month = intval($_POST['month']);

  function season($month) {
    if (7 <= $month && $month <= 9) {
      return $month . "月は夏です。";
    }elseif (10 <= $month && $month <= 11){
      return $month . "月は秋です。";
    }elseif (12 == $month || $month <= 3) {
      return $month . "月は冬です。";
    }elseif (4<= $month && $month <= 6) {
      return $month . "月は春です。";
    }
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php if($p_flg == 0):?>
  <h2>季節</h2>
  <form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
    <select name="month" size="3">
      <?php for ($i=1; $i<=12; $i++):?>
        <option value="<?= $i?>"><?= $i ?>月</option>;
      <?php endfor;?>
    </select>
    <input type="submit" value="送信">
  </form>
  <?php endif;?>
  <?php if($p_flg == 1):?>
  <h2>季節表示</h2>
  <?= season($month);?>
  <?php endif;?>
</body>
</html>
