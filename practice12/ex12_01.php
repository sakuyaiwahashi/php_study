  <?php
  $p_flg = 0;
  $age = "";

  //===== ポスト：リクエスト処理 =====
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $p_flg = 1;
    $age = $_POST['age'];
    function age($age){
      if ($age % 4 == 0 && $age % 100 != 0 || $age % 400 == 0) {
          return $age . "は閏年です。";
      } else {
          return $age . "は閏年ではありません。";
      }
    }
  }


?>



<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>

  <?php if ($p_flg == 0):?>
    <h2>平年/閏年表示</h2>
  <form action="<?= $_SERVER["SCRIPT_NAME"] ?>" method="post">
    <div>
      年：
      <input type="text" name="age" value="<?= $age ?>" size="10">
      <br>
      <input type="submit" name="btn" value="送信">
    </div>
  </form>
  <?php else:?>
    <h2>平年/閏年表示</h2>
    <?= age($age); ?>
  <?php endif;?>
</body>
</html>
