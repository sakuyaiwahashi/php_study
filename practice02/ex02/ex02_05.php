<?php
/*
 演習2-5
   Author:Okamoto
 必要なとき以外は<head>は省略しています。。。
*/
?>
<html>
<head>
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css">
<!--
	td {width:36px; text-align: center;}
	th {width:36px; text-align: center;}
-->
</style>
 
<title>ex02_05.php</title>
</head>
<body>
<h1>演習問題 2-5：11～19の九九</h1>
<table border=1>
	<tr>
		<th>×</th>
		<?php for($i = 11; $i < 20; $i++): ?>
		<th><?php echo $i; ?></th>
		<?php endfor; ?>
	</tr>

	<?php for($i = 11; $i < 20; $i++): ?>
	<tr>
		<th><?php echo $i; ?></th>
		<?php for($j = 11; $j < 20; $j++): ?>
		<td><?php echo $i * $j; ?></td>
		<?php endfor; ?>
	</tr>
	<?php endfor; ?>

</table>
</body>
</html>
