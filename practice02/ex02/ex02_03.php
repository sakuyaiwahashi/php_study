<?php
/*
 演習2-3
   Author:Okamoto
 必要なとき以外は<head>は省略しています。。。

10回、乱数を生成し、その合計値と最大値を表示しなさい
 
*/
?>
<body>
<h1>演習問題 2-3</h1>
<?php
    $total = 0;
    $max = 0;
    for ($i = 0; $i < 10; $i++) {
        $num1 = rand(1, 100);
        echo "$num1 ";
        $total += $num1;
        if ($num1 > $max)
            $max = $num1;
    }
    echo "<br />合計は、${total}<br />最大値は、${max}<br />";
?>
</body>