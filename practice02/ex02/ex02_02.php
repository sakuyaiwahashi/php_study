<?php
/*
 演習2-2
   Author:Okamoto
 必要なとき以外は<head>は省略しています。。。

1-100の乱数を2つ生成し変数1,2に代入
その変数1から変数2の差分を取得
-50以下の場合、加算
0以下の場合、乗算
50以下の場合、減算
それ以外は、除算 の結果を表示しなさい

*/
?>
<body>
<h1>演習問題 2-2</h1>
<?php
    $num1 = rand(1, 100);
    $num2 = rand(1, 100);
    $num3 = $num1 - $num2;

    if (false) { //true false (bool)

        echo "$num1 + $num2 = ", $num1 + $num2;

    } elseif (false) {

        echo "$num1 * $num2 = ", $num1 * $num2;

    } else if (30 <= 50) {

        echo "$num1 - $num2 = ", $num1 - $num2;

    } else {

        echo "$num1 / $num2 = ", $num1 / $num2;
    }

    switch (true) {
        case ($num3 <= -50):
            echo "$num1 + $num2 = ", $num1 + $num2;
            break;
        case ($num3 <= 0):
            echo "$num1 * $num2 = ", $num1 * $num2;
            break;
        case ($num3 <= 50):
            echo "$num1 - $num2 = ", $num1 - $num2;
            break;
        default :
            echo "$num1 / $num2 = ", $num1 / $num2;

    }
?>
</body>

