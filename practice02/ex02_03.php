<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
    $max = 0;
    $total = 0;
    for($i = 1; $i <= 9; $i++){
      $a = rand(1, 100);
      echo "${a} ";
      $total += $a;
      if ($a > $max){
        $max = $a;
      }
    }
    echo "<br>";
    echo "合計値は、${total}";
    echo "<br>";
    echo "最大値は、${max}";
  ?>
</body>
</html>
