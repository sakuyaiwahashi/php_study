<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
    $a = rand(1, 100);
    $b = rand(1, 100);
    $c = $a - $b;
    if ($c <= -50){
      $d = $a + $b;
      echo "${a} + ${b} = ${d}";
      echo "$a + $b =", $a + $b;
    }
    elseif ($c <= 0){
      $d = $a * $b;
      echo "${a} * ${b} = ${d}";
    }
    elseif($c <= 50){
      $d = $a - $b;
      echo "${a} - ${b} = ${d}";
    }
    else{
      $d = $a / $b;
      echo "${a} / ${b} = ${d}";
    }
  ?>
</body>
</html>
