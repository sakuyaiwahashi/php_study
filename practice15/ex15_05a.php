<?php
  $p_flg = 0;

  $db = new PDO("mysql:host=localhost;dbname=practice", "root", "sakuya");

  $sql = 'SELECT DISTINCT deptno FROM emp ORDER BY deptno';

  $result = $db -> query($sql);

  $rows = $result -> fetchall(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php if ($p_flg == 0): ?>
    <form action="ex15_05b.php" method="post">
      部門番号:
      <select class="" name="deptno">
        <?php
          foreach ($rows as $row) {
            foreach ($row as $key => $value) {
              echo "<option value='${value}'>" . $value . "</option>";
            }
          }
        ?>

      </select>
      <br>
      <input type="submit" value="検索">
    </form>
  <?php endif; ?>


</body>
</html>
