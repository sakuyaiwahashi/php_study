<?php
  $p_flg = 0;

  $db = new PDO("mysql:host=localhost;dbname=practice", "root", "sakuya");

  $sql = 'SELECT DISTINCT job FROM emp';
  $result = $db -> query($sql);
  $rows = $result -> fetchall(PDO::FETCH_ASSOC);

  $sql2 = 'SELECT DISTINCT loc FROM dept';
  $result2 = $db -> query($sql2);
  $rows2 = $result2 -> fetchall(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <form action="ex15_06b.php" method="post">
    職種：
    <select class="" name="job">
      <option value="未選択">選択なし</option>
      <?php
        foreach ($rows as $row) {
          foreach ($row as $key => $value) {
            echo "<option value='${value}'>" . $value . "</option>";
          }
        }
      ?>
    </select>
    <br>
    所在地：
    <select class="" name="loc">
      <option value="未選択">選択なし</option>
      <?php
        foreach ($rows2 as $row) {
          foreach ($row as $key => $value) {
            echo "<option value='${value}'>" . $value . "</option>";
          }
        }
      ?>
    </select>
    <br>
    <input type="submit" value="検索">
  </form>
</body>
</html>
