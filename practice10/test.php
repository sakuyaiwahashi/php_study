<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
  $cookie = "repeaters"; // Cookieの名前
  $period = time() + 365*24*3600; // Cookieの有効期限(サンプルは１年)

  if(isset($HTTP_COOKIE_VARS[$cookie])) {
    $num = (int)$HTTP_COOKIE_VARS[$cookie];
    $num++;
  }
  else $num = 1;
  setcookie($cookie, $num, $period);
  ?>
  <?php
  if($num == 1) echo "初めての訪問ありがとうございます";
  else echo "今回で".$num."回目の訪問になります";
  ?>
</body>
</html>
