<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
    <?php if($_SERVER["REQUEST_METHOD"] !== "POST"):?>
    <h2>ニックネームをクッキーに保存</h2>
    <form class="" action="<?= $_SERVER["SCRIPT_NAME"]?>" method="post">
      ニックネーム：
      <input type="text" name="nickname">
      <input type="submit" value="送信">
    </form>
    <?php else:?>
    <?php
      $nickname = $_POST['nickname'];
      setcookie("nickname", $nickname, time() + 60);
    ?>
    <h2>クッキー保存 </h2>
    ニックネームを保存しました
    <?php

      if(isset($_COOKIE['nickname'])){
        $cookie = $_COOKIE['nickname'];
        echo "${cookie}さん、ようこそ！";
      }else{
        $cookie = "";
        echo "クッキーがありません";
      }


    ?>
    <?php endif;?>
</body>
</html>
