<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php if($_SERVER["REQUEST_METHOD"] !== "POST"):?>
  <h2>訪問回数の表示</h2>
  <?php
    if(isset($_COOKIE['kaisu'])){
      $kaisu = $_COOKIE['kaisu'];
      $kaisu++;
      setcookie("kaisu", "${kaisu}", time() + 5);
    }else{
      $kaisu = 1;
      setcookie("kaisu", "${kaisu}", time() + 5);
    }
    echo "今回で".$kaisu."回目の訪問になります";
  ?>
  <?php else:?>
  <?php endif;?>

</body>
</html>
